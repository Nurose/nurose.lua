﻿using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.IO;
using Nurose.Utilities;

namespace Nurose.Lua
{

    public class LuaReader
    {
        public Script script = new Script(CoreModules.Preset_HardSandbox);

        public LuaReader()
        {
            SetGlobalCustomTypes();
        }

        /// <summary>
        /// Register and set the the custom types of Nurose and the Assembly using Nurose as globals in the Lua script environment. 
        /// </summary>
        public void SetGlobalCustomTypes()
        {
            List<Assembly> assemblies = ReflectionSaver.GetAllNuroseAssemblies();

            var types = new List<Type>();
            foreach (var assembly in assemblies)
            {
                types.AddRange(assembly.GetTypes().ToList());
            }

            types.Add(typeof(List<object>).GetGenericTypeDefinition());

            foreach (var type in types)
            {
                UserData.RegisterType(type);
                if (type.IsGenericType)
                {
                    var c = type.Name.Remove(type.Name.Length - 2, 2);
                    script.Globals[type.Name.Remove(type.Name.Length - 2, 2)] = type;

                }
                else
                {

                    script.Globals[type.Name] = type;
                }
            }
            UserData.RegisterAssembly();

        }

        


        /// <summary>
        /// Set a object as a global Lua object in the Lua environment. 
        /// </summary>
        /// <param name="key">Name of the variable in Lua environmont</param>
        /// <param name="value">The object</param>
        public void SetGlobal(string key, object value)
        {
            script.Globals[key] = value;
        }


        public T ExecuteAndReturnAs<T>(string scriptCode)
        {

            return DoString(scriptCode).ToObject<T>();
        }

        public DynValue DoString(string scriptCode)
        {
                return script.DoString(scriptCode);
        }

        public T ExecuteAndReturnAsAsync<T>(string scriptCode)
        {
            return DoStringAsync(scriptCode).ToObject<T>();
        }

        public DynValue DoStringAsync(string scriptCode)
        {
            return  script.DoString(scriptCode);
        }


    }
}
